if has("gui_running")
else
    colorscheme elflord-old
    " Turn transparency back on
    highlight Normal guibg=NONE ctermbg=NONE
    highlight Search cterm=NONE ctermfg=black ctermbg=blue
endif
syntax on

set nocompatible
set noerrorbells

set laststatus=2    " Always show show status line
set noswapfile
set number
set relativenumber
set report=0        " report if files changes
set ruler           " Show cursor position
set sessionoptions=curdir,buffers,tabpages,folds
set signcolumn=yes
set splitbelow
set splitright
set suffixesadd=.js,.jsx,.ts,.tsx
set wildmenu
" incremental substitution (neovim)
if has('nvim')
    set inccommand=split
endif

" Searching
set incsearch   " Search as you type
set gdefault    " /g
set showmatch
set ignorecase
set smartcase

set exrc    " Use local rc files
set secure

set clipboard+=unnamed  " share windows clipboard

" Indention
set autoindent
set smartindent
set smarttab
set tabstop=4
set shiftwidth=4
set backspace=indent,eol,start
set cino=N-s  " Don't indent namespace contents

"set novisualbell
set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]
set iskeyword+=_,$,@,%,#    " none of these are word dividers
set listchars=tab:>-,trail:-" tabs and trailing ws
set listchars+=nbsp:¬
"set list

" Make current line more distinct
set cursorline
" Can't read the purple text with this on:
" highlight LineNr cterm=none ctermfg=240 guifg=#2b506e guibg=#000000
" highlight CursorLine cterm=none ctermbg=DarkGrey guibg=Grey40
" augroup BgHighlight
"     autocmd!
"     autocmd WinEnter * set cul
"     autocmd WinLeave * set nocul
" augroup END

" :Explore uses tree view
let g:netrw_liststyle = 3

" Turn off paste mode when leaving insert
autocmd InsertLeave * set nopaste

if has('nvim')
    set inccommand=nosplit  " This is really cool
end

" Plugins

let s:dein_dir=expand("$DOTFILES/.vim/dein")
let s:dein_plugin_dir=expand('~/.cache/dein')

let &rtp .= ','.expand('~/.vim/dein')

call dein#begin(s:dein_plugin_dir)
    " I've configured it as a submodule; I will manage dein myself
    " call dein#add(s:dein_dir)
    " The following yanked from
    " https://github.com/craftzdog/dotfiles-public/blob/6f415824dbf8637a391cab29e470399880a3e834/.vimrc
    let g:rc_dir = expand('~/.vim/rc')
    let s:toml = g:rc_dir . '/dein.toml'
    let s:lazy_toml = g:rc_dir . '/dein_lazy.toml'
    let s:toml_nvim = g:rc_dir . '/dein_nvim.toml'
    call dein#load_toml(s:toml,      {'lazy': 0})
    call dein#load_toml(s:lazy_toml, {'lazy': 1})
    if has('nvim')
        call dein#load_toml(s:toml_nvim, {'lazy': 0})
    endif
call dein#end()

"if dein#check_install()
"    call dein#install()
"endif

runtime! ftplugin/man.vim

let g:pathogen_disabled = []
"call add(g:pathogen_disabled, 'nerdtree-git-plugin')

let AutoPairsCenterLine = 0

" Shows visible ^G in some fonts
" https://github.com/scrooloose/nerdtree/issues/916
let NERDTreeNodeDelimiter = "\u00a0"

" For vim-system-copy
if !empty($DISPLAY)
    " It uses xsel by default, but I have xclip installed
    let g:system_copy#copy_command='xclip -sel clipboard'
    let g:system_copy#paste_command='xclip -sel clipboard -o'
endif

execute pathogen#infect()
filetype plugin indent on

set rtp+=/usr/bin/fzf

" Show git branch in lightline (from lightline README)
" Hide lightline stuff in narrow windows (from README)
function! LightlineFileformat()
    return winwidth(0) > 70 ? &fileformat : ''
endfunction
function! LightlineFileencoding()
    return winwidth(0) > 70 ? &fileencoding : ''
endfunction
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'fileformat': 'LightlineFileformat',
      \   'fileencoding': 'LightlineFileencoding',
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }
" Turn off extra mode indicator; it's redundant w/ lightline loaded
au VimEnter * if exists('g:loaded_lightline') | set noshowmode | endif

" Key mappings

nnoremap ; :

" Leader
let mapleader = ","
" Copy/paste:
" https://sheerun.net/2014/03/21/how-to-boost-your-vim-productivity/
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P
nnoremap <silent> <Leader>c :set invlist<CR>
" Comment line and duplicate (depends on vim-commentary)
nmap     <silent> <Leader>d yygccp
" nnoremap <silent> <Leader>d yy:call <Plug>CommentaryLine<CR>p
nnoremap <Leader>e :Explore<CR>
" Show buffer list
nnoremap <Leader>l :buffers<CR>:buffer<Space>
" fzf
nmap <Leader>f :FZF<CR>
" fzf.vim :Rg
nmap <Leader>g :Rg<CR>
nnoremap <silent> <Leader>h :set invhlsearch<CR>
nnoremap <Leader>s :mksession!<CR>
" Show tree sidebar
nnoremap <Leader>t :NERDTreeToggle<CR>
nnoremap <F4> :NERDTreeToggle %<CR>

" Emacs/Readline keybindings for commandline mode
" https://github.com/JoshCheek/dotfiles/blob/master/dotfiles/vimrc
cnoremap <C-A> <Home>
cnoremap <C-F> <Right>
cnoremap <C-B> <Left>
cnoremap <Esc>b <S-Left>
cnoremap <Esc>f <S-Right>

nnoremap <F1> :help<space>
imap <F1> <Esc>

" Save
nmap <C-s> :w<CR>
imap <C-s> <ESC>:w<CR>a
" be aware of terminal output suspend on <C-s>

" Search results centered
" https://github.com/jonhoo/configs/blob/master/editor/.config/nvim/init.vim
nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <silent> * *zz
nnoremap <silent> # #zz
nnoremap <silent> g* g*zz
nnoremap ? ?\v
"nnoremap / /\v
cnoremap %s/ %sm/

" Tabs
nmap <C-t> :tabnew<CR>
nmap <C-tab> :tabnext<CR>
nmap <C-S-tab> :tabprevious<CR>
imap <C-t> <ESC>:tabnew<CR>
imap <C-tab> <ESC>:tabnext<CR>
imap <C-S-tab> <ESC>:tabprevious<CR>

" Open tag in new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>

" Generate C++ tags
" TODO:  fix warning:  --extra is deprecated
map <A-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" TagExplorer
nnoremap <silent> <F8> :TagExplorer<CR>
if has("gui_running")
else
    let TE_Adjust_Winwidth = 0
endif

au BufNewFile,BufRead *.nut setf squirrel

" JSON
au! BufRead,BufNewFile *.json set foldmethod=syntax

" .vimrc
au BufRead,BufNewFile .vimrc set expandtab

" HTML
au BufRead,BufNewFile *.html set expandtab shiftwidth=2 softtabstop=2
" Javascript
au BufRead,BufNewFile *.js,*.jsx,*.ts,*.tsx set expandtab shiftwidth=4 softtabstop=4 nowrap

" Ruby
au BufRead,BufNewFile *.rb,*.rhtml set expandtab shiftwidth=2 softtabstop=2 nowrap

" Python
au BufRead,BufNewFile *.py set expandtab

" C++
au BufRead,BufNewFile *.cpp,*.h,*.hpp,*.hh,*.c set expandtab shiftwidth=4 softtabstop=4

" Jabba
au BufRead,BufNewFile *.java set expandtab shiftwidth=4 softtabstop=4

" Prevent accidental writes to buffers that shouldn't be edited
" (https://github.com/jonhoo/configs/blob/master/editor/.config/nvim/init.vim)
autocmd BufRead *.orig set readonly
autocmd BufRead *.pacnew set readonly

"" Visual mode
" Copy / paste
"vmap <C-c> "+y
"nmap <C-v> "+gP

"if has("gui_running")
"    set guifont=Consolas:h8
"endif
