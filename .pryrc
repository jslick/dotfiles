# From https://docs.gitlab.com/ee/development/pry_debugging.html :
if defined?(PryByebug)
  Pry.commands.alias_command 's', 'step'
  Pry.commands.alias_command 'n', 'next'
  Pry.commands.alias_command 'f', 'finish'
  Pry.commands.alias_command 'fin', 'finish'
  Pry.commands.alias_command 'c', 'continue'
  Pry.commands.alias_command 'bt', 'backtrace'
end

# Repeat last command
Pry::Commands.command /^$/, "repeat last command" do
  _pry_.run_command Pry.history.to_a.last
end
