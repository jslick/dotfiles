# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=20000
SAVEHIST=20000
setopt appendhistory autocd extendedglob notify histignorespace
unsetopt BEEP
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/user/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Git branch
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '%b'
setopt PROMPT_SUBST

#autoload -U promptinit && promptinit
#prompt adam1
PROMPT="%F{cyan}%n%f%F{yellow}@%f%F{green}%B%m%b%f %F{red}%B%1~%b%f %F{yellow}%#%f "
RPROMPT="%F{blue}\$vcs_info_msg_0_%f"

export DOTFILES=~/dotfiles

command -v nvim &>/dev/null && export EDITOR=nvim || export EDITOR=vim
# export EDITOR=vim
# export TERMINAL=alacritty
export TERMINAL=kitty

# fzf stuff
export FZF_DEFAULT_OPTS='--height 12'
[ -x "$(command -v fd)" ] && export FZF_DEFAULT_COMMAND='fd --type f --hidden --exclude .git'

[ -f ~/.profile ] && source ~/.profile
source $DOTFILES/aliases.sh
[ -f ~/.aliases.local.sh ] && source ~/.aliases.local.sh
[ -f ~/dotfiles/.zsh-fixkeys ] && source ~/dotfiles/.zsh-fixkeys

if [ -f ~/.fzf.zsh ]; then
    source ~/.fzf.zsh
elif [ -f /usr/share/zsh/vendor-completions/_fzf -a -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
    source /usr/share/zsh/vendor-completions/_fzf
    source /usr/share/doc/fzf/examples/key-bindings.zsh
elif [ -f /usr/share/doc/fzf/examples/completion.zsh -a -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
    source /usr/share/doc/fzf/examples/completion.zsh
    source /usr/share/doc/fzf/examples/key-bindings.zsh
elif [ -f /usr/share/fzf/completion.zsh -a -f /usr/share/fzf/key-bindings.zsh ]; then
    source /usr/share/fzf/completion.zsh
    source /usr/share/fzf/key-bindings.zsh
elif [ -f /usr/share/fzf/key-bindings.zsh ]; then
    source /usr/share/fzf/key-bindings.zsh
fi

if [ -e /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
elif [ -e /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
elif [ -e /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh ]; then
    source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
fi

setopt null_glob
for x in $DOTFILES/shellrc.d/*.zsh; do
    source $x
done
unsetopt null_glob

stty -ixon
