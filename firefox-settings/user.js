// Undo gentoo BS:
user_pref('accessibility.typeaheadfind', false);

// General
user_pref('browser.tabs.tabmanager.enabled', true);
user_pref('browser.vpn_promo.enabled', false);
user_pref('extensions.pocket.enabled', false);
user_pref('extensions.pocket.showHome', false);
user_pref('privacy.query_stripping.enabled', true);
user_pref('privacy.query_stripping.enabled.pbmode', true);
user_pref('toolkit.tabbox.switchByScrolling', true);

// Disable autoplay
user_pref('media.autoplay.blocking_policy', 2);
user_pref('media.autoplay.default', 5);
user_pref('media.block-autoplay-until-in-foreground', false);

// Fix screwed up new style
user_pref('toolkit.legacyUserProfileCustomizations.stylesheets', true);
user_pref('browser.compactmode.show', true);
user_pref('browser.uidensity', 1);

// Gestures nav
user_pref('browser.gesture.swipe.left', '');
user_pref('browser.gesture.swipe.right', '');

// Idiotic spring back scrolling thing copied from apple
user_prof('apz.overscroll.enabled', false);
