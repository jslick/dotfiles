generate: .install/commands.sh

.install/commands.sh: .FORCE
	@mkdir -p .install
	bash prepare.sh > $@
	@chmod +x $@
	cat $@

.FORCE:

clean:
	rm .install/commands.sh
	rmdir .install

.PHONY: .FORCE generate clean
