#!/usr/bin/env sh

# Environment variables
export LESS=-R

[ -z "$PAGER" ] && {
    command -v bat &>/dev/null && PAGER='bat' || PAGER='less'
}

command -v lsd &>/dev/null && alias ls='lsd' || alias ls='ls --color'
alias ll='ls -l'
alias la='ls -a'
alias grep='grep --color'
alias find='find -L'
alias tree='tree -C'
alias lessr='less -r'
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -Iv'
alias diff='diff --color=auto'
alias dmesg='dmesg --color=always'
alias ncdu='ncdu --color dark'
alias e='exa -lgm --git'
alias ea='e -a'
alias rg'rg --hidden --follow --smart-case'

alias reload='source ~/.zshrc && echo "Reloaded RC"'

# Ripped from hexdsl
alias myip="curl http://ipecho.net/plain; echo"
alias motherboard='cat /sys/devices/virtual/dmi/id/board_{vendor,name,version}'

# Places
alias p='cd ~/Documents/programming'
alias notes='cd ~/Documents/notes'
alias dots='cd $HOME/dotfiles'

if [ $SHELL = "/usr/bin/zsh" ]; then
    alias histgrep='history 0 | grep'
else
    alias histgrep='history | grep'
fi

alias vimrc='vim ~/.vimrc'
alias zshrc='$EDITOR -p ~/.zshrc ~/dotfiles/aliases.sh'

calc() { awk "BEGIN { print $* }" ; }
alias clip='xclip -selection clipboard'
alias fullmatrix='xterm -bg black -fg white -fullscreen -e cmatrix & xterm -geometry +1800 -bg black -fg white -fullscreen -e cmatrix &'
alias homeupdatedb='updatedb -l 0 -o ~/locate.db -U ~'
alias homelocate='locate -d ~/locate.db'
alias pagethat='$(fc -ln -1) | $PAGER'
alias urldecode='sed "s@+@ @g;s@%@\\\\x@g" | xargs -0 printf "%b"'
alias watchfreq='watch grep \"cpu MHz\" /proc/cpuinfo'
alias whichgpu="lspci -nnk | grep -i vga -A3 | grep 'in use'"

alias hc='herbstclient'

archage() {
    echo $(($(($(date +%s) - $(date -d "$(head -1 /var/log/pacman.log | cut -d ' ' -f 1,2 | tr -d '[]')" +%s))) / 86400)) days
}

# KDE
alias kdelock='qdbus org.freedesktop.ScreenSaver /ScreenSaver Lock'

# Shortcuts
SC_linksdir=~/dotfiles/shortcuts
sc () {
    local f=`readlink $SC_linksdir/$1`
    [ "$f" != "" ] && cd $f || echo "Link $1 does not exist" >&2
}

# Sets the shell title
# https://superuser.com/a/599156
function title {
    echo -ne "\033]0;"$*"\007"
}

# For VMWare; mount shared folder
vmhg() {
    vmhgfs-fuse -o auto_unmount .host:/ host
}

alias xp='xprop | grep "WM_WINDOW_ROLE\|WM_CLASS" && echo "WM_CLASS(STRING) = \"NAME\", \"CLASS\""'

blankgit=4b825dc642cb6eb9a060e54bf8d69288fbee4904
