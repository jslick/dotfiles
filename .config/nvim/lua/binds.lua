vim.g.mapleader = ','

local keymap = vim.keymap

-- Comment line and duplicate (depends on vim-commentary)
keymap.set('n', '<leader>d', 'yygccp', { silent = true })
-- keymap.set('n', '<leader>e', ':Explore<CR>')
-- Show buffer list
-- Replaced by telescope:
-- keymap.set('n', '<leader>l', ':buffers<CR>:buffer<Space>')
-- toggle highlighted search
keymap.set('n', '<leader>h', ':set invhlsearch<CR>', { silent = true })
keymap.set('n', '<leader>s', ':mksession!<CR>')

-- Readline keybindings for command mode
-- https://github.com/JoshCheek/dotfiles/blob/master/dotfiles/vimrc
keymap.set('c', '<C-A>', '<Home>')
keymap.set('c', '<C-F>', '<Right>')
keymap.set('c', '<C-B>', '<Left>')
keymap.set('c', '<Esc>b', '<S-Left>')
keymap.set('c', '<Esc>f', '<S-Right>')

-- Save
keymap.set('n', '<C-s>', ':w<CR>')
keymap.set('i', '<C-s>', '<Esc>w:<CR>a')

-- Search results centered
-- https://github.com/jonhoo/configs/blob/master/editor/.config/nvim/init.vim
keymap.set('n', 'n', 'nzz', { silent = true })
keymap.set('n', 'N', 'Nzz', { silent = true })
keymap.set('n', '*', '*zz', { silent = true })
keymap.set('n', '#', '#zz', { silent = true })
keymap.set('n', 'g*', 'g*zz', { silent = true })
keymap.set('n', '?', '?\v')
keymap.set('c', '%s/', '%sm/')

-- Tabs
keymap.set('n', '<leader>tt', ':tabnew<CR>')
keymap.set('n', '<leader>tn', ':tabn<CR>')
keymap.set('n', '<leader>tp', ':tabp<CR>')
keymap.set('n', '<leader>td', ':tabnew %<CR>')
