return {
    'lewis6991/gitsigns.nvim',
    event = { 'BufReadPre', 'BufNewFile' },
    opts = {
        on_attach = function(buffer)
            local gs = package.loaded.gitsigns

            local function map(mode, l, r, desc)
                vim.keymap.set(mode, l, r, { buffer = bufnr, desc = desc })
            end

            -- Navigation
            map('n', ']v', gs.next_hunk, 'Next Hunk')
            map('n', '[v', gs.prev_hunk, 'Prev Hunk')

            -- Actions
            map('n', '<leader>vs', gs.stage_hunk, 'Stage hunk')
            map('n', '<leader>vr', gs.reset_hunk, 'Reset hunk')
            map('v', '<leader>vs', function()
                gs.stage_hunk({ vim.fn.line('.'), vim.fn.line('v') })
            end, 'Stage hunk')
            map('v', '<leader>vr', function()
                gs.reset_hunk({ vim.fn.line('.'), vim.fn.line('v') })
            end, 'Reset hunk')

            map('n', '<leader>vS', gs.stage_buffer, 'Stage buffer')
            map('n', '<leader>vR', gs.reset_buffer, 'Reset buffer')

            map('n', '<leader>vu', gs.undo_stage_hunk, 'Undo stage hunk')

            map('n', '<leader>vp', gs.preview_hunk, 'Preview hunk')

            map('n', '<leader>vb', function()
                gs.blame_line({ full = true })
            end, 'Blame line')
            map('n', '<leader>vB', gs.toggle_current_line_blame, 'Toggle line blame')

            map('n', '<leader>vd', gs.diffthis, 'Diff this')
            map('n', '<leader>vD', function()
                gs.diffthis('~')
            end, 'Diff this ~')

            -- Text object
            map({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>', 'Gitsigns select hunk')
        end,
    },
}
