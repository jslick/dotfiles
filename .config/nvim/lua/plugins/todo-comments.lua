return {
    'folke/todo-comments.nvim',
    event = { 'BufReadPre', 'BufNewFile' },
    dependencies = { 'nvim-lua/plenary.nvim' },
    config = true,
    -- config = function()
    --     local todo_comments = require('todo-comments')
    --
    --     todo_comments.setup()
    -- end,
}
