return {
    'nvim-tree/nvim-tree.lua',
    dependencies = 'nvim-tree/nvim-web-devicons',
    config = function()
        local nvimtree = require('nvim-tree')
        local api = require('nvim-tree.api')

        vim.g.loaded_netrw = 1
        vim.g.loaded_netrwPlugin = 1

        local function my_on_attach(buf)
            local function opts(desc)
                return { desc = 'nvim-tree: ' .. desc, buffer = buf, noremap = true, silent = true, nowait = true }
            end

            api.config.mappings.default_on_attach(buf)

            vim.keymap.set('n', '<C-h>', function()
                api.tree.resize({
                    relative = -5,
                })
            end, opts('Dec width'))
            vim.keymap.set('n', '<C-l>', function()
                api.tree.resize({
                    relative = 5,
                })
            end, opts('Inc width'))
        end

        nvimtree.setup({
            actions = {
                open_file = {
                    window_picker = {
                        enable = false,
                    },
                },
            },
            view = {
                width = {
                    min = 30,
                    max = 55,
                },
            },
            renderer = {
                icons = {
                    glyphs = {
                        folder = {
                            arrow_closed = '',
                            arrow_open = '',
                        },
                    },
                },
            },
            filters = {
                custom = { 'Session.vim' },
            },
            git = {
                ignore = false,
            },
            on_attach = my_on_attach,
        })

        local keymap = vim.keymap -- for conciseness

        -- https://github.com/josean-dev/dev-environment-files/blob/main/.config/nvim/lua/josean/plugins/nvim-tree.lua
        keymap.set('n', '<leader>ee', '<cmd>NvimTreeToggle<CR>', { desc = 'Toggle file explorer' })
        keymap.set('n', '<leader>ef', '<cmd>NvimTreeFindFile<CR>', { desc = 'Toggle file explorer on current file' })
        keymap.set('n', '<leader>ec', '<cmd>NvimTreeCollapse<CR>', { desc = 'Collapse file explorer' })
        keymap.set('n', '<leader>er', '<cmd>NvimTreeRefresh<CR>', { desc = 'Refresh file explorer' })
    end
}
