return {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
        'nvim-lua/plenary.nvim',
        { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
        'nvim-tree/nvim-web-devicons',
        'folke/todo-comments.nvim',
    },
    config = function()
        local telescope = require('telescope')
        local actions = require('telescope.actions')

        telescope.setup({
            defaults = {
                path_display = { 'smart' },
                vimgrep_arguments = {
                    'rg',
                    '--color=never',
                    '--no-heading',
                    '--with-filename',
                    '--line-number',
                    '--column',
                    '--smart-case',
                    '--hidden',
                },
                -- mappings = {
                -- },
            },
        })

        telescope.load_extension('fzf')

        local builtin = require('telescope.builtin')
        local function find_files()
            return builtin.find_files({ hidden=true })
        end
        -- vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Telescope files' })
        vim.keymap.set('n', '<leader>ff', find_files, { desc = 'Telescope files' })
        vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = 'Telescope grep' })
        vim.keymap.set('n', '<leader>fc', builtin.grep_string, { desc = 'Telescope grep string' })
        vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Telescope buffers' })
        vim.keymap.set('n', '<leader>fr', builtin.oldfiles, { desc = 'Telescope recent files' })
        vim.keymap.set('n', '<leader>fm', builtin.marks, { desc = 'Telescope marks' })
        vim.keymap.set('n', '<leader>fd', builtin.commands, { desc = 'Telescope commands' })
        vim.keymap.set('n', '<leader>fj', builtin.jumplist, { desc = 'Telescope jumplist' })
        vim.keymap.set('n', '<leader>fq', builtin.quickfix, { desc = 'Telescope quickfix list' })
        vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Telescope help tags' })
        vim.keymap.set('n', '<leader>fsd', builtin.lsp_document_symbols, { desc = 'Telescope symbols in doc' })
        vim.keymap.set('n', '<leader>fsw', builtin.lsp_document_symbols, { desc = 'Telescope symbols in workspace' })
        vim.keymap.set('n', '<leader>ft', ':TodoTelescope<CR>', { desc = 'Telescope TODOs' })
        vim.keymap.set('n', '<leader>fy', ':Telescope yank_history<CR>', { desc = 'Telescope yank history' })
    end,
}
