-- Pane for treesitter symbols
return {
    'stevearc/aerial.nvim',
    opts = {},
    dependencies = {
        'nvim-treesitter/nvim-treesitter',
        'nvim-tree/nvim-web-devicons',
    },
    config = function()
        local aerial = require('aerial')

        aerial.setup({
            on_attach = function(buf)
                -- Jump forwards/backwards with '{' and '}'
                vim.keymap.set('n', '{', '<cmd>AerialPrev<CR>', { buffer = buf })
                vim.keymap.set('n', '}', '<cmd>AerialNext<CR>', { buffer = buf })
            end,
        })

        vim.keymap.set('n', '<leader>a', '<cmd>AerialToggle<CR>')
    end,
}
