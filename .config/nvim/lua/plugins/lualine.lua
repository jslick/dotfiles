return {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    config = function()
        local lualine = require('lualine')
        local autoTheme = require('lualine.themes.auto')
        -- I customized the listchars color (vim:NonText,
        -- tokyonight:colors.fg_gutter) so that they would be more visible, but
        -- it impacts the lualine theme.  This resets it to the default color:
        autoTheme.normal.a.fg = '#3b4261'
        autoTheme.normal.b.bg = '#3b4261'
        autoTheme.insert.a.fg = '#3b4261'
        autoTheme.insert.b.bg = '#3b4261'
        autoTheme.replace.a.fg = '#3b4261'
        autoTheme.replace.b.bg = '#3b4261'
        autoTheme.visual.a.fg = '#3b4261'
        autoTheme.visual.b.bg = '#3b4261'
        autoTheme.command.a.fg = '#3b4261'
        autoTheme.command.b.bg = '#3b4261'
        autoTheme.terminal.a.fg = '#3b4261'
        autoTheme.terminal.b.bg = '#3b4261'
        autoTheme.inactive.a.fg = '#3b4261'
        autoTheme.inactive.b.bg = '#3b4261'

        lualine.setup({
            options = {
                theme = autoTheme,
                -- theme = 'onedark',
            },
        })
    end,
}
