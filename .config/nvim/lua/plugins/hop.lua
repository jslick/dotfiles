-- Alternative: https://github.com/ggandor/leap.nvim
return {
    'smoka7/hop.nvim',
    version = '*',
    config = function()
        local hop = require('hop')

        hop.setup({
            keys = 'etovxqpdygfblzhckisuran',
        })

        vim.keymap.set('n', '<leader>j', function()
            vim.cmd('HopWord')
        end, { remap = true })
    end,
}
