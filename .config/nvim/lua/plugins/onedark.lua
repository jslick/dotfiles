-- There's a bunch of these; idk which one to choose:
--  * https://github.com/navarasu/onedark.nvim
--  * https://github.com/olimorris/onedarkpro.nvim
--  * https://github.com/joshdick/onedark.vim
return {
    'navarasu/onedark.nvim',
    enabled = false,
    config = function()
        local onedark = require('onedark')

        onedark.setup({
            transparent = true,
            lualine = {
                transparent = true,
            },
        })

        vim.cmd('colorscheme onedark')
    end,
}
