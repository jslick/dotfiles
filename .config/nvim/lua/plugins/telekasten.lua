-- Note taking
return {
    'nvim-telekasten/telekasten.nvim',
    config = function()
        local telekasten = require('telekasten')

        telekasten.setup({
            home = vim.fn.expand('~/Documents/telekasten'),
        })
    end,
}
