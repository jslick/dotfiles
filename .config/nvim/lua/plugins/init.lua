return {
    'nvim-lua/plenary.nvim',  -- lua functions that many plugins use
    'RRethy/vim-illuminate',
    'machakann/vim-swap',
    -- 'tpope/vim-commentary',
}
