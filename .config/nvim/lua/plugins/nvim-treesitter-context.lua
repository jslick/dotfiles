return {
    'nvim-treesitter/nvim-treesitter-context',
    config = function()
        local tsc = require('treesitter-context')

        tsc.setup({})

        vim.keymap.set('n', '[c', function()
            tsc.go_to_context(vim.v.count1)
        end, { silent = true, desc = 'Go to current context' })
    end,
}
