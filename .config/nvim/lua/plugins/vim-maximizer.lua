return {
    'szw/vim-maximizer',
    keys = {
        { '<leader>mm', ':MaximizerToggle<CR>', desc = 'Max/Min a split' },
    },
}
