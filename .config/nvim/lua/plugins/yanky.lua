return {
    'gbprod/yanky.nvim',
    dependencies = { 'nvim-telescope/telescope.nvim' },
    config = function()
        local yanky = require('yanky')
        local yankyPicker = require('yanky.picker')
        local telescope = require('telescope')

        yanky.setup({
            picker = {
                select = {
                    -- TODO:  not working:
                    action = yankyPicker.actions.set_register('"'),
                },
            },
        })

        telescope.load_extension('yank_history')
    end,
}
