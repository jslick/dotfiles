return {
    'folke/tokyonight.nvim',
    priority = 1000,
    config = function()
        require("tokyonight").setup {
            transparent = true,
            styles = {
               sidebars = "transparent",
               floats = "transparent",
            },
            -- @param colors ColorScheme
            on_colors = function(colors)
                -- colors.fg_gutter = '#3b4261'  -- default value
                colors.fg_gutter = '#5b6281'  -- brightening the default value
            end,
        }

        vim.cmd('colorscheme tokyonight')
    end
}
