return {
    'kylechui/nvim-surround',
    event = { 'BufReadPre', 'BufNewFile' },
    version = '*',
    config = true,
    -- ys<motion><char> : surround object with <char>
    -- ds<motion><char> : delete surrounding, <char> from object
    -- cs<motion><char1><char2> : change surrounding <char1> to <char2>
    -- t : HTML tag
}
