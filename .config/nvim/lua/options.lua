vim.cmd('let g:netrw_liststyle = 3')

local opt = vim.opt

opt.swapfile = false
opt.backspace = 'indent,eol,start'
opt.cursorline = true
opt.number = true
opt.relativenumber = true
-- none of these are word dividers
opt.iskeyword:append('_,$,@,%,#')

-- formatting
opt.autoindent = true
opt.tabstop = 4
opt.shiftwidth = 4

-- indentation characters
opt.list = true
opt.listchars = { tab = '>-', trail = '-', nbsp = '¬' }

-- searching
opt.ignorecase = true
opt.inccommand = 'split'
opt.incsearch = true
opt.gdefault = true
opt.showmatch = true
opt.smartcase = true

-- theme support
opt.termguicolors = true
opt.background = 'dark'
opt.signcolumn = 'yes'

-- copy/paste
-- use system clipboard
opt.clipboard:append('unnamedplus')

-- window splits
opt.splitbelow = true
opt.splitright = true

-- auto commands

vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
    pattern = { '*.lua', '*.html', '*.py', '*.cpp', '*.h', '*.hpp', '*.hh', '*.c', '*.java', '*.kt', '*.kdl' },
    callback = function(evt)
        vim.bo.expandtab = true
    end,
})

vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, {
    pattern = { '*.js', '*.jsx', '*.ts', '*.tsx' },
    callback = function(evt)
        vim.bo.expandtab = true
        vim.o.wrap = false
    end,
})

-- Prevent accidental writes to buffers that shouldn't be edited
-- (https://github.com/jonhoo/configs/blob/master/editor/.config/nvim/init.vim)
vim.api.nvim_create_autocmd({ 'BufRead' }, {
    pattern = { '*.orig', '*.pacnew' },
    callback = function(evt)
        vim.o.readonly = true
    end,
})
