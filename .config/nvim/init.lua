-- Blended my old vim config with:
-- https://github.com/josean-dev/dev-environment-files/tree/aa7778ffa4ebe0b9040e1805e49591bc7171024d/.config/nvim
require('options')
require('binds')
require('lazy-bootstrap')
