#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

[ -f ./environment.sh ] && source ./environment.sh
source $SCRIPT_DIR/install-helpers.sh

use_imply i3    plain-wm
use_imply dwm   plain-wm
use_imply hlwm  plain-wm
use_imply hikari plain-wayland
use_imply hyprland plain-wayland

use_imply plain-wm       gui
use_imply plain-wayland  gui
use_imply plasma         gui

symlink .gitignore
symlink .tmux
symlink .tmux/tmux.conf .tmux.conf
symlink .vim
symlink .vimrc
symlink .config/nvim
symlink .zshrc
symlink_force .config/htop
symlink_force .config/neofetch
symlink .config/zellij
#symlink .config/skippyxd

is_using gui && {
    symlink .config/alacritty
    symlink .config/kitty
    symlink .config/sxhkd
}

is_using plain-wm && {
    symlink .Xresources
    symlink .config/dunst
    symlink_force .config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
}

is_using plain-wayland && {
    symlink .config/dunst
    symlink .config/foot
    symlink .config/kitty
    symlink .config/waybar
}

is_using i3 && {
    symlink .config/i3
    symlink .config/jgmenu
    symlink .config/polybar
}

is_using hlwm && {
    symlink .config/herbstluftwm
    # NOTE:  Using xfce4-panel instead when available
    #        But, barpyrus would require less deps
    symlink .config/barpyrus
}

is_using hikari && {
    symlink .config/hikari
}

is_using hyprland && {
    symlink .config/hypr
}

is_using lxqt && {
    symlink .config/lxqt
    symlink .config/openbox
}

is_using plasma && {
    symlink .config/breezerc
    symlink .config/kcminputrc
    symlink .config/kglobalshortcutsrc
    symlink .config/konsolerc
    symlink .local/share/konsole
    symlink .local/share/color-schemes/BreezeDarkCustom.colors
    symlink .config/yakuakerc
    symlink .config/kwinrc
    symlink .config/kxkbrc
    symlink .config/plasma-workspace
}

is_using rubydev && symlink .pryrc

for i in ${!tasks[@]}; do
    task=${tasks[$i]}
    echo "$task"
done
