#!/usr/bin/env bash

# TODO:  remove bash-isms

if [[ `grep -i thinkpad /sys/devices/virtual/dmi/id/product_version` ]]; then
	setxkbmap -model thinkpad dvorak
else
	setxkbmap dvorak
fi
xset r rate 220 30
# Turn off beep
xset -b
if [[ $( command -v synclient ) ]]; then
	synclient TapButton1=1
	synclient TapButton2=3
	synclient TapButton3=2
	synclient MinSpeed=3.7
	synclient MaxSpeed=0.7
	synclient AccelFactor=0.0358423
	synclient MaxTapMove=245
fi
xinput set-prop 'SynPS/2 Synaptics TouchPad' 'libinput Tapping Enabled' 1
xinput set-prop 'SynPS/2 Synaptics TouchPad' 'libinput Accel Speed' 0.6
xinput set-prop 'SynPS/2 Synaptics TouchPad' 'libinput Disable While Typing Enabled' 0
