#!/bin/sh

redshift &
setxkbmap dvorak
nitrogen --restore &
xset r rate 220 30
tilda &
sxhkd &
# unclutter &
exec mate-session
# exec ck-launch-session dbus-launch mate-session
