#!/bin/sh

redshift &
setxkbmap dvorak
nitrogen --restore &
xset r rate 220 30
if [ command -v synclient ]; then
	synclient TapButton1=1
	synclient TapButton2=3
	synclient TapButton3=2
fi
picom &
# xdg-screensaver lock seems to be broken and I don't see a GUI option somewhere to change it.  Just use this instead
xss-lock -- ~/dotfiles/lock-command.sh &
tilda &
sxhkd &
# unclutter &
exec dbus-launch --exit-with-session startlxqt
