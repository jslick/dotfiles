#!/bin/sh

if [ -d /etc/X11/xinit/xinitrc.d ]; then
	for f in /etc/X11/xinit/xinitrc.d/*; do
		[ -x "$f" ] && . "$f"
	done
	unset f
fi

[ -e ~/.Xresources ] && xrdb -merge ~/.Xresources

systemctl --user start pulseaudio &
redshift &
source $HOME/dotfiles/xinitrc/input-settings.sh
nitrogen --restore &
picom &
slstatus &
dunst &
xfce4-power-manager &
xfce4-volumed-pulse &
# Run sxhkd after dwm so that dwm keybindings take precedence
(sleep 0.25 && (sxhkd &)) &
xss-lock -- ~/dotfiles/lock-command.sh &
tilda &
unclutter -idle 5 &
exec dwm
