#!/usr/bin/env bash

redshift &
source $HOME/dotfiles/xinitrc/input-settings.sh
nitrogen --restore &
tilda &
sxhkd &
# unclutter &
exec cinnamon-session
