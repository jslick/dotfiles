#!/usr/bin/env bash
# Helpers to prepare a dotfiles installation

src=${INSTALL_SRC:-$PWD}
prefix=${INSTALL_DEST:-$HOME}
use=${USE:-}

tasks=()
declare -A using

log() {
    >&2 echo "$@"
}

# create the using lookup table
for word in $use; do
    using["$word"]=1
done

log "src=$src"
log "prefix=$prefix"
log "using ${!using[@]}"

is_using() {
    name=$1
    [[ ${using[$name]} -eq 1 ]]
}

# using one feature implies another
use_imply() {
    p=$1
    q=$2
    is_using $p && using["$q"]=1
}

add_task() {
    tasks[${#tasks[@]}]="${*}"
}

# Make a symlink if the destination does not already exist
symlink() {
    name=$1
    link_target=$1
    link_dest=${2:-$name}
    [ ! -e $( dirname $prefix/$link_dest ) ] && \
        add_task mkdir -p $( dirname $prefix/$link_dest )
    [ ! -e $prefix/$link_dest ] && \
        add_task ln -s $src/$link_target $prefix/$link_dest
}

# Make a symlink if the destination does not already match the desired target
symlink_force() {
    name=$1
    link_target=$1
    link_dest=${2:-$name}
    [ ! -e $( dirname $prefix/$link_dest ) ] && \
        add_task mkdir -p $( dirname $prefix/$link_dest )
    [[ ! -e $prefix/$link_dest || "`readlink $prefix/$link_dest`" != "$src/$link_target" ]] && \
        add_task ln -sf $src/$link_target $prefix/$link_dest
}
