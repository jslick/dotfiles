-- Window management --
WINDOW_ANIMATION = 0.2

hs.hotkey.bind({"alt", "shift"}, "s", function()
    local win = hs.window.focusedWindow()
    -- http://www.hammerspoon.org/docs/hs.window.html#moveOneScreenEast
    win:moveOneScreenEast({
        noResize = 1,
        ensureInScreenBounds = 1,
        duration = WINDOW_ANIMATION,
    })
end)

hs.hotkey.bind({"alt", "shift"}, "h", function()
    local win = hs.window.focusedWindow()
    win:moveOneScreenWest({
        noResize = 1,
        ensureInScreenBounds = 1,
        duration = WINDOW_ANIMATION,
    })
end)

function findOrLaunch(a)
    local app = hs.application.find(a)
    if not app then
        hs.application.launchOrFocus(a)
    end
    return hs.application.find(a)
end

-- Hammerspoon stuff --

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "W", function()
    hs.notify.new({title="Hammerspoon", informativeText="Hello World"}):send()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "R", function()
    hs.reload()
end)
hs.alert.show("Hammerspood config loaded")

-- Applications --

hs.hotkey.bind({"alt"}, "I", function()
    firefox = findOrLaunch("Firefox")
    firefox:selectMenuItem({"File", "New Window"})
    firefox:activate()
end)

hs.hotkey.bind({"alt","shift"}, "I", function()
    firefox = findOrLaunch("Firefox")
    firefox:selectMenuItem({"File", "New Private Window"})
    firefox:activate()
end)

hs.hotkey.bind({"cmd","ctrl"}, "T", function()
    iterm = findOrLaunch("iTerm2")
    iterm:selectMenuItem({"Shell", "New Window"})
    iterm:activate()
end)

hs.hotkey.bind({"alt"}, "S", function()
    slack = hs.appfinder.appFromName("Slack")
    slack:activate()
end)

hs.hotkey.bind({"alt"}, "O", function()
    -- TODO
end)

-- wireless watcher from hammerspoon guide
wifiWatcher = nil
homeSSID = "devnull5"
lastSSID = hs.wifi.currentNetwork()

function ssidChangedCallback()
    newSSID = hs.wifi.currentNetwork()

    if newSSID == homeSSID and lastSSID ~= homeSSID then
        -- We just joined our home WiFi network
        hs.audiodevice.defaultOutputDevice():setVolume(25)
    elseif newSSID ~= homeSSID and lastSSID == homeSSID then
        -- We just departed our home WiFi network
        hs.audiodevice.defaultOutputDevice():setVolume(0)
    end

    lastSSID = newSSID
end

wifiWatcher = hs.wifi.watcher.new(ssidChangedCallback)
wifiWatcher:start()
