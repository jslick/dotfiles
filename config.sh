#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source $SCRIPT_DIR/install-helpers.sh

symlink .gitignore
symlink .tmux
symlink .tmux/tmux.conf .tmux.conf
symlink .vim
symlink .vimrc
symlink .zshrc
symlink .config/alacritty
symlink_force .config/htop
symlink_force .config/neofetch
#symlink .config/skippyxd
symlink .config/sxhkd

is_using i3 && {
    symlink .config/i3
    symlink .config/jgmenu
    symlink .config/polybar
}

is_using lxqt && {
    symlink .config/lxqt
    symlink .config/openbox
}

is_using plasma && {
    symlink .config/breezerc
    symlink .config/kcminputrc
    symlink .config/konsolerc
    symlink .config/kwinrc
    symlink .config/kxkbrc
}

is_using plain-wm && {
    symlink .Xresources
    symlink .config/dunst

}

is_using rubydev && symlink .pryrc

for i in ${!tasks[@]}; do
    task=${tasks[$i]}
    echo "$task"
done
