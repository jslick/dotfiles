#!/usr/bin/env sh

DOTFILES="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo "Current directory is $DOTFILES"

ln -s $DOTFILES/.gitconfig ~/.gitconfig
ln -s $DOTFILES/.gitignore ~/.gitignore

ln -s $DOTFILES/.tmux ~/.tmux
ln -s ~/.tmux/tmux.conf ~/.tmux.conf

ln -s $DOTFILES/.vim ~/.vim
ln -s $DOTFILES/.vimrc ~/.vimrc
ln -s $DOTFILES/.gvimrc ~/.gvimrc

ln -s $DOTFILES/.zshrc ~/.zshrc

ln -s $DOTFILES/.taskrc ~/.taskrc

mkdir ~/.config/htop
ln -s $DOTFILES/.config/htop/htoprc ~/.config/htop/htoprc

ln -s $DOTFILES/.config/neofetch ~/.config/neofetch

ln -s $DOTFILES/.config/alacritty ~/.config/alacritty

ln -s $DOTFILES/.config/dunst ~/.config/dunst

ln -s $DOTFILES/.config/openbox ~/.config/openbox

ln -s $DOTFILES/.config/lxqt ~/.config/lxqt

if [ -e ~/.config/termite ]; then
    (>&2 echo '~/.config/termite already exists')
else
    ln -s $DOTFILES/.config/termite ~/.config/termite
fi
